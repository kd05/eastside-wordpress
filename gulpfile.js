const gulp = require('gulp');
const uglify = require('gulp-uglify-es').default;
const sourcemaps = require('gulp-sourcemaps');
const sass = require('gulp-sass');
const concat = require('gulp-concat');

const themeAssets = './wp-content/themes/eastside/assets/';


/*
  -- TOP LEVEL FUNCTIONS --
  gulp.task - Define tasks
  gulp.src - Point to files to use
  gulp.dest - Points to folder to output
  gulp.watch - Watch files and folders for changes
*/

// Logs Message
gulp.task('message', function(){
    return console.log('Gulp is running...');
});




gulp.task('sass', function () {
    return gulp.src(themeAssets+'/scss/style.scss')
        .pipe(sourcemaps.init())
        .pipe(sass.sync({outputStyle: 'compact'}).on('error', sass.logError))
        .pipe(concat('style.min.css'))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest(themeAssets+'dist/css/'));
});



// JS Compress and Concate
gulp.task('scripts', function(){
    return gulp.src(themeAssets+'js/*.js')
                .pipe(concat('main.js'))
                .pipe(sourcemaps.init())
                .pipe(uglify())
                .pipe(sourcemaps.write())
                .pipe(gulp.dest(themeAssets+'dist/js/'));
});





gulp.task('default', gulp.parallel(['message',  'scripts', 'sass']));




// ********   Run Command "gulp watch" and save js and css   ********
gulp.task('watch', function(){
    gulp.watch(themeAssets+'js/*.js', gulp.parallel(['scripts']));
    gulp.watch(themeAssets+'scss/**/*.scss', gulp.parallel(['sass']));
});
