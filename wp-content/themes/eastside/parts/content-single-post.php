<?php
/**
 * Template part for displaying posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.2
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>




    <?php
    $banner_bg_img_array = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full' );
    $banner_bg_img = is_array($banner_bg_img_array) ? current($banner_bg_img_array) : get_default_banner();
    ?>
    <div class="common-banner-page"  style=" background-image: url('<?php echo $banner_bg_img; ?>')">
        <div class="common-banner-gradient"></div>
        <div class="info-content">
            <?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
        </div>
    </div>



	<div class="entry-content">
		<?php
		/* translators: %s: Name of current post */
		the_content( sprintf(
			__( 'Continue reading<span class="screen-reader-text"> "%s"</span>', 'twentyseventeen' ),
			get_the_title()
		) );

		wp_link_pages( array(
			'before'      => '<div class="page-links">' . __( 'Pages:', 'twentyseventeen' ),
			'after'       => '</div>',
			'link_before' => '<span class="page-number">',
			'link_after'  => '</span>',
		) );
		?>
	</div><!-- .entry-content -->



</article><!-- #post-## -->
