<?php
get_header();
?>

<section>
    <article class="">

        <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

            <?php the_content(); ?>

        <?php endwhile; endif; ?>
    </article>
</section>



<?php get_footer(); ?>
