
jQuery(document).ready(function($) {


    // **************************************************
    // **********     Initialize Map & Style      *******
    // **************************************************
    const snazzy_map_style = function()  {
        return [
            {
                "featureType": "all",
                "elementType": "labels.text",
                "stylers": [
                    {
                        "lightness": "8"
                    },
                    {
                        "gamma": "0.00"
                    }
                ]
            },
            {
                "featureType": "all",
                "elementType": "labels.text.fill",
                "stylers": [
                    {
                        "saturation": 36
                    },
                    {
                        "color": "#000000"
                    },
                    {
                        "lightness": "65"
                    }
                ]
            },
            {
                "featureType": "all",
                "elementType": "labels.text.stroke",
                "stylers": [
                    {
                        "visibility": "on"
                    },
                    {
                        "color": "#000000"
                    },
                    {
                        "lightness": 16
                    }
                ]
            },
            {
                "featureType": "all",
                "elementType": "labels.icon",
                "stylers": [
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "featureType": "administrative",
                "elementType": "geometry.fill",
                "stylers": [
                    {
                        "color": "#000000"
                    },
                    {
                        "lightness": 20
                    }
                ]
            },
            {
                "featureType": "administrative",
                "elementType": "geometry.stroke",
                "stylers": [
                    {
                        "color": "#000000"
                    },
                    {
                        "lightness": 17
                    },
                    {
                        "weight": 1.2
                    }
                ]
            },
            {
                "featureType": "landscape",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#000000"
                    },
                    {
                        "lightness": 20
                    }
                ]
            },
            {
                "featureType": "landscape",
                "elementType": "geometry.fill",
                "stylers": [
                    {
                        "color": "#242424"
                    }
                ]
            },
            {
                "featureType": "poi",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#000000"
                    },
                    {
                        "lightness": 21
                    }
                ]
            },
            {
                "featureType": "poi",
                "elementType": "geometry.fill",
                "stylers": [
                    {
                        "color": "#292929"
                    }
                ]
            },
            {
                "featureType": "road.highway",
                "elementType": "geometry.fill",
                "stylers": [
                    {
                        "color": "#121212"
                    },
                    {
                        "lightness": "0"
                    },
                    {
                        "gamma": "1.00"
                    }
                ]
            },
            {
                "featureType": "road.highway",
                "elementType": "geometry.stroke",
                "stylers": [
                    {
                        "color": "#000000"
                    },
                    {
                        "lightness": "16"
                    },
                    {
                        "weight": 0.2
                    }
                ]
            },
            {
                "featureType": "road.arterial",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#000000"
                    },
                    {
                        "lightness": 18
                    }
                ]
            },
            {
                "featureType": "road.arterial",
                "elementType": "geometry.fill",
                "stylers": [
                    {
                        "color": "#191919"
                    },
                    {
                        "gamma": "0.00"
                    },
                    {
                        "lightness": "8"
                    }
                ]
            },
            {
                "featureType": "road.arterial",
                "elementType": "labels.text",
                "stylers": [
                    {
                        "lightness": "4"
                    }
                ]
            },
            {
                "featureType": "road.local",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#000000"
                    },
                    {
                        "lightness": 16
                    }
                ]
            },
            {
                "featureType": "transit",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#000000"
                    },
                    {
                        "lightness": 19
                    }
                ]
            },
            {
                "featureType": "water",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#000000"
                    },
                    {
                        "lightness": 17
                    }
                ]
            },
            {
                "featureType": "water",
                "elementType": "geometry.fill",
                "stylers": [
                    {
                        "color": "#141414"
                    }
                ]
            }
        ];
    }

    function initMap() {
        const mapContainer = document.getElementById('map');
        const latitude = parseFloat(mapContainer.getAttribute('data-latitude'));
        const longitude = parseFloat(mapContainer.getAttribute('data-longitude'));
        const zoom = parseInt(mapContainer.getAttribute('data-zoom'));
        const icon = mapContainer.getAttribute('data-icon');
        const address_title = mapContainer.getAttribute('data-address-title');
        const address = mapContainer.getAttribute('data-address');
        // console.log(latitude,longitude,website_urls.image_url);
        let location = {lat: latitude, lng: longitude};
        var map = new google.maps.Map(
            mapContainer,
            {
                zoom: zoom,
                center: location,
                disableDefaultUI: true,
                styles : snazzy_map_style()
            });

        var marker = new google.maps.Marker({position: location, map: map, icon: icon});
        var mapPopupContent = `<div class="map-popup-info"><h6>${address_title}</h6> <p>${address}</p></div>`;


        // Info Window
        let posx;
        let posy
        if(window.innerWidth > 1366){ posx = 164;  posy = 100; }
        else{  posx = 140;  posy = 100; }

        // console.log(window.innerWidth, posx, posy);

        var infowindow = new google.maps.InfoWindow({
            content: mapPopupContent,
            pixelOffset: new google.maps.Size(posx, posy)
        });
        infowindow.open(map, marker);
    }

    if($(".map-wrapper").length) {
        initMap();
    }


    const map_resize_generate = function (e) {
        // console.log(e, window.innerWidth);
        initMap();
    };
    window.addEventListener("resize", map_resize_generate);

});