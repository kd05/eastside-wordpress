// Aos Animation
AOS.init({
    duration: 1200, // values from 0 to 3000, with step 50ms
    once: true
});


jQuery(document).ready(function($) {

    // **************************************************
    // **********  Moby Menu   *******
    // **************************************************
    var mobyMenu = new Moby({
        menu        : jQuery('.top-right ul.menu'), // The menu that will be cloned
        mobyTrigger : jQuery('#moby-button'), // Button that will trigger the Moby menu to open
        subMenuOpenIcon  : '<i class="material-icons">&#xE313;</i>',
        subMenuCloseIcon : '<i class="material-icons">&#xE316;</i>',
    });



    // **************************************************
    // **********  Header Color change on Scroll  *******
    // **************************************************
    const header_top = 320;
    const header = document.querySelector(".site-header");

    document.addEventListener("scroll", (e) => {
        const scroll_top = window.scrollY;
        if(scroll_top > header_top) { header.classList.add("dark"); }
        else { header.classList.remove("dark"); }
    });




    // **************************************************
    // **********    Home Page Rotator (Slick) **********
    // **************************************************
    const getTwoDigitNumber = function (num) {
        if(num < 10 ) return `0${num}`;
        else return num;
    }


    if($(".homeslider-outer-wrapper").length){
        const home_slide_num = document.querySelector(".homeslider-outer-wrapper .slide-number");
        const home_total_slide = document.querySelectorAll(".home-slider-container .home-slides").length;
        const getTotalSlide = getTwoDigitNumber(home_total_slide);

        // Initial Set Slide to 1
        home_slide_num.querySelector("span").textContent  = `01 / ${getTotalSlide}`;

        // Init Home Slide
        $('.home-slider-container')
            .slick({
                autoplay: true,
                autoplaySpeed: 15000,
                arrows: true,
                dots: false,
                fade: true,
                speed: 900,
            });

        //***************  On Slider change Update Home Rotator Number  ***************
        $('.home-slider-container').on('init reInit afterChange', function(event, slick, currentSlide, nextSlide){
            var i = (currentSlide ? currentSlide : 0) + 1;
            let currentSlideNum = getTwoDigitNumber(i);
            home_slide_num.querySelector("span").textContent  = `${currentSlideNum} / ${getTotalSlide}`;
        });
    }







    // **************************************************
    // **********    Services Slide (Slick) **********
    // **************************************************

    if($(".benefits-section").length){
        const service_slide_num = document.querySelector(".benefits-slider-wrapper .slide-number");
        const service_total_slide = document.querySelectorAll(".benefits-slider-container .benefit-slide").length;
        const getTotalServiceSlide = getTwoDigitNumber(service_total_slide);

        // Initial Set Service Slide to 1
        service_slide_num.querySelector("span").textContent  = `01 / ${getTotalServiceSlide}`;

        // Init Service Slide
        $('.benefits-slider-container')
            .slick({
                dots: false,
                arrows: true,
                // infinite: false,
                speed: 900,
                slidesToShow: 3,
                slidesToScroll: 1,
                responsive: [
                    {
                        breakpoint: 1200,
                        settings: {
                            slidesToShow: 2,
                        }
                    },
                ]
            });

        //***************  On Slider change Update Service Slider Number  ***************
        $('.benefits-slider-container').on('init reInit afterChange', function(event, slick, currentSlide, nextSlide){
            var j = (currentSlide ? currentSlide : 0) + 1;
            let currentServiceSlideNum = getTwoDigitNumber(j);
            service_slide_num.querySelector("span").textContent  = `${currentServiceSlideNum} / ${getTotalServiceSlide}`;
        });
    }





    // **************************************************
    // **********    News Blog Slide (Slick) **********
    // **************************************************
    const news_device_width = 1200;
    function mobileNewsSlider() {

        if($(".news-slider").length){
            $(".news-slider").slick({
                dots: false,
                infinite: true,
                slidesToShow: 3,
                slidesToScroll: 1,
                responsive: [
                    {
                        breakpoint: 960,
                        settings: {
                            slidesToShow: 2,
                        }
                    },
                ]
            });
        }

    }
    // console.log(window.innerWidth,news_device_width);
    if(window.innerWidth < news_device_width) {
        // console.log("here");
        mobileNewsSlider();
    }

    $(window).resize(function(e){
        if(window.innerWidth < news_device_width) {
            setTimeout(function () {
                    if(!$('.news-slider').hasClass('slick-initialized')){
                        mobileNewsSlider();
                    }
            },100);


        }else{
            setTimeout(function () {
                if($('.news-slider').hasClass('slick-initialized')){
                    $('.news-slider').slick('unslick');
                }
            },100);
        }
    });





    /**
     * Contact Us Form
     */

    const contact_form = document.querySelector("#contact-form");

    if(contact_form){
        contact_form.addEventListener('submit', function (event) {
            event.preventDefault();
            var error = 0;
            var msg = "";

            $('.contact-form .chk-email').each(function(){
                var fieldValue = $(this).val();
                var re = /\S+@\S+\.\S+/;
                if(re.test(fieldValue) == false){
                    error = 1;
                    msg = "<p>Please Enter Valid Email Address</p>";
                }
            });

            $('.contact-form .required').each(function(){
                var fieldValue = $(this).val();
                if(fieldValue == ""){
                    error = 1;
                    msg = "<p>Please Enter all the Fields (*)</p>";
                }
            });

            if(error == 1){
                set_message_alert(msg);
                scroll_to("message-alert");
            } else {
                set_message_alert("");
                var security = $("#security").val();
                var data = $('#contact-form').serialize();
                show_loading();
                $.ajax({
                    url: website_urls.ajax_url,
                    type: 'POST',
                    data : {
                        action : 'contact_form',
                        data : data,
                        security : security,
                    },
                    success: function(result){
                        // console.log(result);
                        hide_loading();
                        set_message_alert("<p class='success'>Your Inquiry is Successfully Submitted</p>");
                        scroll_to("message-alert");
                        $('#contact-form').trigger("reset");
                    }
                });
            }

        });

    }







    function set_message_alert(msg){
        $('.message-alert').html(msg);
    }


    function scroll_to(class_scroll){
        var topToScrollTo = $("."+class_scroll).offset().top;
        $("html, body").stop().animate({ scrollTop: topToScrollTo - 100}, 1000);
    }

    function show_loading(){
        $("body").css("opacity",0.6);
        $(".loading-image").removeClass("hidden");
    }

    function hide_loading() {
        $("body").css("opacity", 1);
        $(".loading-image").addClass("hidden");
    }



});