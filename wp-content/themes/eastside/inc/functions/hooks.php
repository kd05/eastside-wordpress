<?php
if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}



//************************************************************
//  Hide Admin Bar Front Side
//************************************************************
add_filter('show_admin_bar', '__return_false');



//************************************************************
//  Disable Auto Update
//************************************************************
add_filter( 'auto_update_plugin', '__return_false' );