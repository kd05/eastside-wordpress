<?php
if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}



// Register menus
register_nav_menus(
	array(
		'main-nav'          => __('The Main Menu', 'eastside'),
		'footer-left' => __('Footer Left Menu', 'eastside'), // footer nav
		'footer-right'  => __('Footer Right Menu', 'eastside'), // footer nav
	)
);

