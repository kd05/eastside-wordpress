<?php
if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}





function gp_content_panel_shortcode( $atts, $content = null ) {
    $a = shortcode_atts(array(
        'title' => 'BENEFITS THAT MATTER'
    ), $atts);

    $title = $a['title'];
    ob_start();

    ?>
    <div class="content-panel-section">
        <div class="cp-left" data-aos="fade-right">
            <h6><?php  echo $title; ?></h6>
        </div>
        <div class="cp-right" data-aos="fade-left">
            <?php echo do_shortcode($content); ?>
        </div>
    </div>
    <?php
    $output = ob_get_clean();
    return $output;
}

add_shortcode( 'gpContentPanel', 'gp_content_panel_shortcode' );
