<?php
if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}





function gp_service_panel_shortcode( $atts, $content = null ) {
    $a = shortcode_atts(array(
        'title' => 'BENEFITS THAT MATTER',
        'button_text' => 'LEARN MORE',
    ), $atts);

    $title = $a['title'];

    ob_start();
    $args = array(
        'numberposts' => -1,
        'fields' =>"ids",
        'orderby' => 'menu_order',
        'order' => 'ASC',
        'post_type' => 'gp-service'
    );
    $services = get_posts($args);
    ?>
    <div class="benefits-section">
        <div class="benefits-section-wrapper">
            <div class="sl-content overlay-colourfull-before"  data-aos="fade-right">
                <div class="sl-content-inner ">
                    <div class="benefit-title">
                        <h2><?php echo $title; ?></h2>
                    </div>
                </div>
            </div>

            <div class="benefits-slider-wrapper"  data-aos="fade-left">
                <div class="slide-number"><span></span></div>
                <div class="benefits-slider-container" >
                    <?php  foreach($services as $serviceId) {
                        $slide = get_the_post_thumbnail_url( $serviceId, 'full') ;
                        $serviceTitle = get_the_title($serviceId);
                        $serviceLink = get_the_permalink($serviceId);
                        ?>
                        <div class="benefit-slide">
                            <div class="benefit-slide-inner" style=" background-image: url('<?php echo $slide; ?>')">
                                <div class="black-gradient"></div>

                                <div class="slide-content">
                                    <h6><?php echo $serviceTitle; ?></h6>
                                    <div class="btn-wrapper">
                                        <a href="<?php echo $serviceLink; ?>" class="btn black">LEARN MORE</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
    <?php
    $output = ob_get_clean();
    return $output;
}

add_shortcode( 'gpServicesPanel', 'gp_service_panel_shortcode' );
