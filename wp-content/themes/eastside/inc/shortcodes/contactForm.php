<?php
if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}




function contact_form_shortcode( $atts, $content = null ) {
    $a = shortcode_atts(array(
        'title' => 'INQUIRY'
    ), $atts);
    $title = $a['title'];
    ob_start();
    ?>
    <form id="contact-form" class="contact-form">
        <h5><?php echo $title; ?></h5>

        <div class="message-alert"></div>

        <input type="hidden" id="security" value="<?php echo wp_create_nonce('security-contact-nonce'); ?>">

        <!-- Submit button-->
        <div class="input-wrapper">
            <label for="full-name">Full Name*</label>
            <input type="text"  class="required" placeholder="Full Name*"  name="full-name" id="full-name" value="">
        </div> <!-- /input-wrapper -->

        <div class="input-wrapper">
            <label for="email-address">Email*</label>
            <input type="email"  class="required chk-email" placeholder="Email*" name="email-address" id="email-address" value="">
        </div> <!-- /input-wrapper -->

        <div class="input-wrapper">
            <label for="phone-no">Phone Number*</label>
            <input type="text"  class="required" placeholder="Phone Number*" name="phone-no" id="phone-no" value="">
        </div> <!-- /input-wrapper -->

        <div class="input-wrapper">
            <label for="message">Your Message</label>
            <textarea name="message" placeholder="Your Message" id="message"></textarea>
        </div> <!-- /input-wrapper -->

        <div class="button-wrapper">
            <input type="submit" value="Send" tabindex="5" id="contact-submit" name="contact-submit" class="thread-button button black" />
        </div> <!-- /button-wrapper -->
    </form>
    <?php
    $output = ob_get_clean();
    return $output;
}
add_shortcode( 'contactForm', 'contact_form_shortcode' );

