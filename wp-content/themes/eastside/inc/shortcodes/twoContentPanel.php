<?php
if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}






function two_content_section_shortcode( $atts, $content = null ) {
    $a = shortcode_atts(array(
    ), $atts);
    ob_start();
    ?>
    <div class="two-content-panel-container">
        <div class="two-content-panel" >
            <?php echo do_shortcode($content); ?>
        </div>
    </div>

    <?php
    $output = ob_get_clean();
    return $output;
}
add_shortcode( 'twoContentPanel', 'two_content_section_shortcode' );




function two_content_single_section_shortcode( $atts, $content = null ) {
    $a = shortcode_atts(array(
        'text' => '',
        'link' => '',
        'color' => 'black',
    ), $atts);
    ob_start();
    ?>
    <div class="two-content-single-panel" >
        <?php echo do_shortcode($content); ?>
    </div>
    <?php
    $output = ob_get_clean();
    return $output;
}
add_shortcode( 'twoContentSinglePanel', 'two_content_single_section_shortcode' );






