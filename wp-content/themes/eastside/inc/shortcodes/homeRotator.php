<?php
if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}




function gp_home_rotator_shortcode( $atts, $content = null ) {
    ob_start();
    $args = array(
        'numberposts' => -1,
        'fields' =>"ids",
        'orderby' => 'menu_order',
        'order' => 'ASC',
        'post_type' => 'gp-rotator'
    );
    $rotators = get_posts($args);
    ?>
    <div class="homeslider-outer-wrapper">
        <div class="home-slider-container" >
            <?php  foreach($rotators as $rotatorId) {
                $slide = get_the_post_thumbnail_url( $rotatorId, 'full') ;
                ?>
                <div class="home-slides"  style=" background-image: url('<?php echo $slide; ?>')">
                    <div class="black-gradient"></div>
                    <?php echo do_shortcode(apply_filters('the_content', get_post_field('post_content', $rotatorId))); ?>
                </div>
            <?php } ?>
        </div>
        <div class="slide-number"><span></span></div>
    </div>
    <?php
    $output = ob_get_clean();
    return $output;
}
add_shortcode( 'gpHomeRotator', 'gp_home_rotator_shortcode' );






function gp_home_rotator_content_shortcode( $atts, $content = null )
{
    $a = shortcode_atts(array(
        'title' => '',
        'text' => '',
        'link' => '',
        'color' => 'black',
    ), $atts);
    ob_start();
    ?>
    <div class="slider-content">
        <div class="sl-content overlay-colourfull-before" >
            <h1><?php echo $a['title']; ?></h1>
        </div>

        <div class="sr-content" >
            <?php echo do_shortcode($content); ?>
        </div>
    </div>
    <?php
    $output = ob_get_clean();
    return $output;
}

add_shortcode( 'gpHomeRotatorContent', 'gp_home_rotator_content_shortcode' );
