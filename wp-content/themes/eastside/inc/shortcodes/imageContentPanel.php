<?php
if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}




function gp_image_content_panel_shortcode( $atts, $content = null ) {
    $a =  shortcode_atts( array(
        'image' => '124',
        'align' => '',
    ), $atts );
    ob_start();

    $main_img = $a['image'];
    $main_img = wp_get_attachment_url($main_img);
    $main_img = "background-image: url($main_img);";
    $main_img_style = "style='$main_img '";

    ?>
    <div class="image-content-panel <?php echo $a['align']; ?>">
        <div class="info-container" data-aos="fade-right">
            <?php echo do_shortcode($content); ?>
        </div>
        <div class="image-container" data-aos="fade-left">
            <div class="img-bg" <?php echo $main_img_style; ?>></div>
        </div>
    </div>
    <?php
    $output = ob_get_clean();
    return $output;
}
add_shortcode( 'imageContentPanel', 'gp_image_content_panel_shortcode' );