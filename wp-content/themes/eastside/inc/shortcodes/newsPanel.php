<?php
if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}




function gp_news_panel_shortcode( $atts, $content = null ) {
    $a = shortcode_atts(array(
        'title' => 'LATEST NEWS',
        'title_align' => 'right',
        'button_text' => 'READ ARTICLE',
    ), $atts);

    ob_start();

    $args = array(
        'numberposts' => 4,
        'orderby' => 'post_date',
        'order' => 'DESC',
        'suppress_filters' => false,
        'post_type' => 'post'
    );
    $recent_news = wp_get_recent_posts( $args);
    $btn_text = $a['button_text'];
    $title_align = $a['title_align'];

    ?>
    <div class="news-wrapper" >
        <div class="news-title <?php echo $title_align; ?>"  data-aos="fade-up">
            <h2><?php echo $a['title']; ?></h2>
        </div>
        <div class="news-container-outer" >
            <div class="news-slider">
                <?php
                $animationTime = 800;
                foreach($recent_news as $news){
                    $news_id = $news['ID'];
                    $news_date = date("m.d.Y", strtotime($news['post_date']));
                    $news_title = $news['post_title'];
                    $news_excerpt = gp_excerptize($news['post_content'], 16);
                    $news_link = get_the_permalink($news_id);
                    ?>
                    <div class='single-news'  data-aos="fade-up" data-aos-easing="linear" data-aos-duration="<?php echo $animationTime; ?>">
                        <div class='news-content'>
                            <a href='<?php echo $news_link; ?>'><h6><?php echo $news_title; ?></h6></a>
                            <p class="date"><?php echo $news_date; ?></p>
                            <p><?php echo $news_excerpt; ?></p>
                            <?php  echo do_shortcode("[gpButton text='$btn_text' link='$news_link']"); ?>
                        </div>
                    </div>
                <?php  $animationTime += 350; }   ?>
            </div>
        </div>
    </div>
    <?php
    $output = ob_get_clean();
    return $output;
}
add_shortcode( 'gpNewsPanel', 'gp_news_panel_shortcode' );



