<?php
if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}



function gp_full_width_image_content_panel_shortcode( $atts, $content = null ) {
    $a = shortcode_atts(array(
        'title' => 'BENEFITS THAT MATTER',
        'image' => '123',
    ), $atts);

    $title = $a['title'];

    $bg_main_img = $a['image'];
    $bg_main_img = wp_get_attachment_url($bg_main_img);
    $bg_main_img = "background-image: url($bg_main_img);";
    $main_img_style = " style='$bg_main_img ' ";
    ob_start();

    ?>
    <div class="full-width-content-panel" <?php echo $main_img_style; ?>>
        <div class="common-banner-gradient"></div>
        <div class="fw-content-panel-section" >
            <div class="cp-left" data-aos="fade-right">
                <h6><?php  echo $title; ?></h6>
            </div>
            <div class="cp-right" data-aos="fade-left">
                <?php echo do_shortcode($content); ?>
            </div>
        </div>
    </div>

    <?php
    $output = ob_get_clean();
    return $output;
}

add_shortcode( 'gpfullWidthImageContentPanel', 'gp_full_width_image_content_panel_shortcode' );
