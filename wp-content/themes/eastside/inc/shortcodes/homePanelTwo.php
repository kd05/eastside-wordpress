<?php
if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}




function home_panel_two_shortcodes( $atts, $content = null ) {

    $a = shortcode_atts(array(
        'main_image' => '70',
        'main_image_title' => 'PROVIDING A WORLD TO YOU',
        'second_image' => '71',
    ), $atts);
    ob_start();


    $bg_main_img = ($a['main_image']) ? $a['main_image'] : false;
    if ($bg_main_img) {
        $bg_main_img = wp_get_attachment_url($bg_main_img);
        $bg_main_img = "background-image: url($bg_main_img);";
    }
    $main_img_style = "style='$bg_main_img '";
    $main_image_title = !empty($a['main_image_title']) ? $a['main_image_title'] : "";


    $second_img = ($a['second_image']) ? $a['second_image'] : false;
    if ($second_img) {
        $second_img = wp_get_attachment_url($second_img);
        $second_img = "background-image: url($second_img);";
    }
    $second_img_style = "style='$second_img '";

    ?>
    <div class="home-panel-two-section" >
        <div class="home-panel-two-wrapper">
            <div class="hpt-left-content overlay-colourfull-before"  data-aos="fade-right">
                <div class="hpt-left-inner-content">
                    <?php echo do_shortcode($content); ?>
                </div>
            </div>
            <div class="hpt-right-content"  data-aos="fade-left">
                <div class="main-img" <?php  echo $main_img_style; ?>>
                    <h2><?php  echo $main_image_title; ?></h2>
                </div>
                <?php  if($second_img) { ?>
                    <div class="second-img" <?php echo $second_img_style; ?>></div>
                <?php } ?>
            </div>
        </div>
    </div>
    <?php
    $output = ob_get_clean();
    return $output;
}
add_shortcode( 'homePanelTwo', 'home_panel_two_shortcodes' );