<?php
if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}




function gp_container_panel_shortcode( $atts, $content = null ) {
    $a = shortcode_atts(array(
    ), $atts);
    ob_start();
    ?>
    <div class="common-container-panel" >
        <?php echo do_shortcode($content); ?>
    </div>
    <?php
    $output = ob_get_clean();
    return $output;
}
add_shortcode( 'containerPanel', 'gp_container_panel_shortcode' );

