<?php
if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}




function gp_map_panel_shortcode( $atts, $content = null ) {
    $a = shortcode_atts(array(
        'title' => 'COME SEE FOR YOURSELF',
        'latitude' => '43.758324',
        'longitude' => '-79.291210',
        'zoom' => '2',
        'map_icon' => '115',
        'address_title' => 'EASTSIDE STUDIOS',
        'address' => '1510 Birchmount Road <br>Toronto, Ontario',
        'facebook_link' => '',
        'twitter_link' => '',
        'youtube_link' => '',
    ), $atts);

    ob_start();

    $title = $a['title'];
    $latitude = $a['latitude'];
    $longitude = $a['longitude'];
    $zoom = $a['zoom'];
    $icon = $a['map_icon'];
    $icon_img = wp_get_attachment_url($icon);
    $address_title = $a['address_title'];
    $address = $a['address'];
    $facebook_link = $a['facebook_link'];
    $twitter_link = $a['twitter_link'];
    $youtube_link = $a['youtube_link'];

    ?>
    <div class="map-wrapper">
        <div class="map-inner-wrapper">
            <div class="map-info"  data-aos="fade-right">
                <h2><?php echo $title; ?></h2>
                <?php echo do_shortcode($content); ?>
                <ul class='social'>
                    <?php if (!empty($facebook_link)) { ?>
                        <li class="item">
                            <a href="<?php echo $facebook_link; ?>" target="_blank" title="Facebook">
                                <i class="fa fa-facebook"></i>
                            </a>
                        </li>
                    <?php } ?>
                    <?php if (!empty($twitter_link)) { ?>
                        <li class="item">
                            <a href="<?php echo $twitter_link; ?>" target="_blank" title="Twitter">
                                <i class="fa fa-twitter"></i>
                            </a>
                        </li>
                    <?php } ?>
                    <?php if (!empty($youtube_link)) { ?>
                        <li class="item">
                            <a href="<?php echo $youtube_link; ?>" target="_blank" title="YouTube">
                                <i class="fa fa-youtube"></i>
                            </a>
                        </li>
                    <?php } ?>
                </ul>
            </div>

            <div class="map-container"  data-aos="fade-left">
                <div class="snazzy-map-container overlay-colourfull-before">
                    <div id="map" class="snazzy-map"
                         data-icon="<?php echo $icon_img; ?>"
                         data-zoom="<?php echo $zoom; ?>"
                         data-latitude="<?php echo $latitude; ?>"
                         data-longitude="<?php echo $longitude; ?>"
                         data-address-title="<?php echo $address_title; ?>"
                         data-address="<?php echo $address; ?>">
                    </div>
                </div>

            </div>
        </div>
    </div>
    <?php
    $output = ob_get_clean();
    return $output;
}
add_shortcode( 'gpMapPanel', 'gp_map_panel_shortcode' );



