<?php
if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}




function gp_button_container_shortcode( $atts, $content = null ) {
    $a = shortcode_atts(array(
    ), $atts);
    ob_start();
    ?>
    <div class="btn-container" >
        <?php echo do_shortcode($content); ?>
    </div>
    <?php
    $output = ob_get_clean();
    return $output;
}
add_shortcode( 'gpButtonContainer', 'gp_button_container_shortcode' );


function gp_button_shortcode( $atts, $content = null ) {
    $a = shortcode_atts(array(
        'text' => '',
        'link' => '',
        'color' => 'black',
    ), $atts);
    ob_start();
    ?>
    <div class="btn-wrapper" >
        <a href="<?php echo $a['link']; ?>" class="btn <?php echo $a['color']; ?>"><?php echo $a['text']; ?></a>
    </div>
    <?php
    $output = ob_get_clean();
    return $output;
}
add_shortcode( 'gpButton', 'gp_button_shortcode' );