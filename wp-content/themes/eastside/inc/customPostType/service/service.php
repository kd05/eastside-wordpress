<?php
if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}



function services_post()
{

    register_post_type('gp-service',
        // let's now add all the options for this post type
        array('labels'              => array(
            'name'               => __('Services', 'eastside'), /* This is the Title of the Group */
            'singular_name'      => __('Service', 'eastside'), /* This is the individual type */
            'all_items'          => __('All Service', 'eastside'), /* the all items menu item */
            'add_new'            => __('Add New', 'eastside'), /* The add new menu item */
            'add_new_item'       => __('Add New Service', 'eastside'), /* Add New Display Title */
            'edit'               => __('Edit', 'eastside'), /* Edit Dialog */
            'edit_item'          => __('Edit Services', 'eastside'), /* Edit Display Title */
            'new_item'           => __('New Services', 'eastside'), /* New Display Title */
            'view_item'          => __('View Services', 'eastside'), /* View Display Title */
            'search_items'       => __('Search Services', 'eastside'), /* Search Custom Type Title */
            'not_found'          => __('Nothing found in the Database.', 'eastside'), /* This displays if there are no entries yet */
            'not_found_in_trash' => __('Nothing found in Trash', 'eastside'), /* This displays if there is nothing in the trash */
            'parent_item_colon'  => ''
        ), /* end of arrays */
            'description'         => __('These are all the store Services', 'eastside'), /* Custom Type Description */
            'public'              => true,
            'publicly_queryable'  => true,
            'exclude_from_search' => true,
            'show_ui'             => true,
            'query_var'           => true,
            'menu_position'       => 11, /* this is what order you want it to appear in on the left hand side menu */
            'menu_icon'           => 'dashicons-book', /* the icon for the custom post type menu. uses built-in dashicons (CSS class name) => dashicons-book */

            'supports'      => array( 'title', 'editor', 'thumbnail', 'excerpt' ,'page-attributes'),
            'rewrite'             => array('slug' => 'gp-service', 'with_front' => false), /* you can specify its url slug */
            'has_archive'         => 'custom_type', /* you can rename the slug here */
            'capability_type'     => 'post',
            'hierarchical'        => false,
            /* the next one is important, it tells what's enabled in the post editor */

        ) /* end of options */
    ); /* end of register post type */


}

// adding the function to the Wordpress init
add_action('init', 'services_post');


add_theme_support( 'post-thumbnails', array( 'gp-service') );