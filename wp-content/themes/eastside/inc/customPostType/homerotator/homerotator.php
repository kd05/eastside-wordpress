<?php
if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}



function rotators_post()
{

    register_post_type('gp-rotator',
        // let's now add all the options for this post type
        array('labels'              => array(
            'name'               => __('Rotator', 'eastside'), /* This is the Title of the Group */
            'singular_name'      => __('Rotator', 'eastside'), /* This is the individual type */
            'all_items'          => __('All Rotator', 'eastside'), /* the all items menu item */
            'add_new'            => __('Add New', 'eastside'), /* The add new menu item */
            'add_new_item'       => __('Add New Rotator', 'eastside'), /* Add New Display Title */
            'edit'               => __('Edit', 'eastside'), /* Edit Dialog */
            'edit_item'          => __('Edit Rotators', 'eastside'), /* Edit Display Title */
            'new_item'           => __('New Rotators', 'eastside'), /* New Display Title */
            'view_item'          => __('View Rotators', 'eastside'), /* View Display Title */
            'search_items'       => __('Search Rotators', 'eastside'), /* Search Custom Type Title */
            'not_found'          => __('Nothing found in the Database.', 'eastside'), /* This displays if there are no entries yet */
            'not_found_in_trash' => __('Nothing found in Trash', 'eastside'), /* This displays if there is nothing in the trash */
            'parent_item_colon'  => ''
        ), /* end of arrays */
            'description'         => __('These are all the store Rotators', 'eastside'), /* Custom Type Description */
            'public'              => true,
            'publicly_queryable'  => true,
            'exclude_from_search' => true,
            'show_ui'             => true,
            'query_var'           => true,
            'menu_position'       => 11, /* this is what order you want it to appear in on the left hand side menu */
            'menu_icon'           => 'dashicons-book', /* the icon for the custom post type menu. uses built-in dashicons (CSS class name) => dashicons-book */

            'supports'      => array( 'title', 'editor', 'thumbnail', 'excerpt' ,'page-attributes'),
            'rewrite'             => array('slug' => 'gp-rotator', 'with_front' => false), /* you can specify its url slug */
            'has_archive'         => 'custom_type', /* you can rename the slug here */
            'capability_type'     => 'post',
            'hierarchical'        => false,
            /* the next one is important, it tells what's enabled in the post editor */

        ) /* end of options */
    ); /* end of register post type */


}

// adding the function to the Wordpress init
add_action('init', 'rotators_post');



add_theme_support( 'post-thumbnails', array( 'gp-rotator','ttttt' ) );