<?php
if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}



Class Email_Templates
{
    public $user_id;
    public $admin_email;
    public $from_email;
    public $subject;
    public $email_header;
    public $header;
    public $body;
    public $footer;


    public function __construct()
    {
        $this->admin_email = get_option("admin_email");
        $this->from_email = get_option("admin_email");
        $this->email_header[] = "From: ".get_option("blogname")."  <eastsidenoreply@gmail.com>";
        $this->email_header[] = "Return-Path: <$this->admin_email>";
        $this->email_header[] = 'MIME-Version: 1.0' . "\r\n";
        $this->email_header[] = 'Content-Type: text/html; charset=UTF-8';


        $this->header = '<table  border="1"  cellpadding="0" cellspacing="0"  width="800">
                            <tr>
                                <td align="left"  bgcolor="#091115" style="padding: 40px 20px">
                                    <img src="' . get_template_directory_uri() . '/assets/images/logo.png." >
                                 </td>
                            </tr>';

        $this->body = '';

        $this->footer = '<tr>
                            <td align="center" style="padding: 20px 0 20px 0;">
                                <span>&copy;  '. get_option("blogname").' '. date("Y") . '</span>
                            </td>
                        </tr>
                    </table>';




    }


    //   Contest Mode Status Change Notify all users
    public function email_contact_us($data)
    {
        $userFullName = $data['full-name'];
        $userEmail = $data['email-address'];
        $phone = $data['phone-no'];
        $message = $data['message'];

        $this->from_email = $userEmail;
        $this->subject = get_option("blogname")." - New Inquiry From User";

        $this->body = '<td bgcolor="#ffffff" style="padding: 70px 30px ;">
                         <table  cellpadding="0" cellspacing="0" width="100%">
                              <tr>
                               <td>
                                    <h3> Hello, Admin</h3>
                                    
                                    <p>A new Inquiry from a new user.</p>
                                    <br>
                                    <p><b>Name: </b>'.$userFullName.'</p>
                                    <p><b>Email: </b>'.$userEmail.'</p>
                                    <p><b>Phone: </b>'.$phone.'</p>
                                    <p><b>Message: </b>'.$message.'</p>
                               </td>                              
                              </tr>
                         </table>
                        </td>';

        $email_html = $this->header .
                      $this->body .
                      $this->footer;

        wp_mail($this->admin_email, $this->subject, $email_html, $this->email_header);
        echo $this->admin_email.$this->subject.$email_html.$this->email_header;
    }

}

// Initialize
global $email_template;
$email_template = new Email_Templates;