<?php
if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}



class GP_Ajax
{
    /**
     * GP_Ajax constructor.
     */
    public function __construct()
    {
        add_action('wp_ajax_contact_form', array($this, 'contact_form'));
    }


    function contact_form()
    {
        check_ajax_referer('security-contact-nonce', 'security');
        global $email_template;
        $params = $_POST['data'];
        parse_str($params, $data);
        $email_template->email_contact_us($data);
        exit;
    }

}



// Initialize
global $gp_ajax;
$gp_ajax = new GP_Ajax();