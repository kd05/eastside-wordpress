<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Nineteen
 * @since 1.0.0
 */

?>

        </div><!-- #content -->
    </div><!-- .site-content-contain -->


	<footer id="colophon" class="site-footer">
		<div class="footer-container">
            <div class="fl">
                <div class="fl-logo">
                    <a href="<?php echo get_home_url(); ?>">
                        <img src="<?php echo get_template_directory_uri() ;?>/assets/images/logo.png" alt="Eastside Studios Logo">
                    </a>
                </div>
                <div class="fl-menu">
                    <?php
                    wp_nav_menu(array(
                        'container'      => false,
                        'theme_location' => 'footer-left'
                    ));
                    ?>
                </div>
            </div>
            <div class="fr">
                <div class="fr-menu">
                    <?php
                    wp_nav_menu(array(
                        'container'      => false,
                        'theme_location' => 'footer-right'
                    ));
                    ?>
                </div>
                <div class="fr-copyright">
                    <p class="copyright">Copyright © <?php echo date("Y"); ?> Eastside Studios </p>
                    <p>Website by GeekPower Web <a target="_blank" href="https://www.in-toronto-web-design.ca/">Design In Toronto</a></p>
                </div>
            </div>
        </div>
	</footer><!-- #colophon -->


</div><!-- #page -->


<img src="<?php echo get_template_directory_uri(); ?>/assets/images/loading.gif" class="loading-image hidden" alt="Dots">
<?php wp_footer(); ?>


</body>
</html>
