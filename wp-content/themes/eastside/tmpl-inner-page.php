<?php
/**
 * Template Name: Inner Page
 * Template Post Type: Page
 *
 */

get_header(); ?>

    <div id="single-post">
        <?php
        /* Start the Loop */
        while ( have_posts() ) : the_post();
            get_template_part( 'parts/content', 'inner-page' );
        endwhile; // End of the loop.
        ?>
    </div>

<?php get_footer();