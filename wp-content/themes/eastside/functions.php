<?php


// ************    Inc / Functions  ********************
require_once(get_template_directory() . '/inc/functions/enqueue-scripts.php');
require_once(get_template_directory() . '/inc/functions/menu.php');
require_once(get_template_directory() . '/inc/functions/hooks.php');
require_once(get_template_directory() . '/inc/functions/gp-functions.php');



// ************    Classes / Ajax  ********************
require_once(get_template_directory() . '/inc/classes/email-templates.php');
require_once(get_template_directory() . '/inc/classes/gp-ajax.php');



// ************   CPT setup     ********************
require_once(get_template_directory() . '/inc/customPostType/homerotator/homerotator.php');
require_once(get_template_directory() . '/inc/customPostType/service/service.php');




// ************   Shortcodes     ********************
require_once(get_template_directory() . '/inc/shortcodes/buttonContainer.php');
require_once(get_template_directory() . '/inc/shortcodes/contactForm.php');
require_once(get_template_directory() . '/inc/shortcodes/containerPanel.php');
require_once(get_template_directory() . '/inc/shortcodes/contentPanel.php');
require_once(get_template_directory() . '/inc/shortcodes/fullWidthImageContent.php');
require_once(get_template_directory() . '/inc/shortcodes/homePanelTwo.php');
require_once(get_template_directory() . '/inc/shortcodes/homeRotator.php');
require_once(get_template_directory() . '/inc/shortcodes/imageContentPanel.php');
require_once(get_template_directory() . '/inc/shortcodes/mapPanel.php');
require_once(get_template_directory() . '/inc/shortcodes/newsPanel.php');
require_once(get_template_directory() . '/inc/shortcodes/servicesPanel.php');
require_once(get_template_directory() . '/inc/shortcodes/twoContentPanel.php');





