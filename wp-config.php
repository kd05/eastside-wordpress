<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'eastside' );

/** MySQL database username */
define( 'DB_USER', 'eastside' );

/** MySQL database password */
define( 'DB_PASSWORD', 'geekpower1@' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'Aw#o;~^$4H>ibkiAM|e<1]]n,W/-wRb)C=rx_{Y5N)2Gg|a(Gkkll!pzvI7&6}R-' );
define( 'SECURE_AUTH_KEY',  '=upaaM5^~N^2K~YYSo^{(p=]xxin]Hd^<hYG_6,lr4c>=81v4.6@H@u.zX616O^|' );
define( 'LOGGED_IN_KEY',    'nx5r>38Bn@jjyoGSVz-(!;KR@5QXjGO!F_3z ES_Ee@o9tgn|EWr@fWJw.D]BRA5' );
define( 'NONCE_KEY',        ',F]iRPO^igEQZ*383)_H!]+4Ivh1`E@:QTuJRSmXxSa(`GF#f].kS|MF#IrF(y4#' );
define( 'AUTH_SALT',        'SJTxI 4y53iuQ{G2#aCyTA4rrJmlhv;_-2w4jsC,1&E{U2jdZUTD6`0 T;a!|DF ' );
define( 'SECURE_AUTH_SALT', 'vrN[kFODw(wBiK}Vv-D{}1:&X=.W9pvtM(5.H{nu;`_ogh|xoR<AQtq^sWn^v] `' );
define( 'LOGGED_IN_SALT',   'T2wB`Pp3ntBeH=3ZC!M=R1cor(vV%V#~wX-`=Q)rd-~<+u_?*,Uj=X[0yVYIALg@' );
define( 'NONCE_SALT',       'bmd=o/E]Y~BoYI{/Tx.j`*gqTK,`:8|GUi3`mk;!2mSp@8&~4}_ je >Ma];w1_M' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
